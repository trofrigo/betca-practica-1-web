import {Component, Inject, OnInit} from '@angular/core';
import { ThemeApiService } from './theme-api.service';
import {Theme} from './theme';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-theme-detail',
  templateUrl: './theme-detail.component.html',
  styleUrls: ['./theme-detail.component.css']
})
export class ThemeDetailComponent implements OnInit {

  private theme: Theme;

  constructor(
    private http: ThemeApiService,
    public dialogRef: MatDialogRef<ThemeDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.theme = data || {
      id: null,
      name: '',
      description: ''
    };
  }

  ngOnInit() { }

  create() {
    if (this.checkForm()) {
      this.http.create(this.theme).subscribe(
        () => { this.dialogRef.close(); });
    }

  }

  update() {
    if (this.checkForm()) {
      this.http.update(this.theme).subscribe(
        () => { this.dialogRef.close(); });
    }
  }

  private checkForm() {
    return (<HTMLFormElement>document.getElementById('detail-form')).checkValidity();
  }

  private isUpdating() {
    return !!this.theme.id;
  }

}
