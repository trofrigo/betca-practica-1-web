import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { ThemeApiService } from './theme-api.service';
import { Theme } from './theme';
import { MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-theme-list',
  templateUrl: './theme-list.component.html',
  styleUrls: ['./theme-list.component.css']
})

export class ThemeListComponent implements OnInit {

  @Output()
  editTheme: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  deleteTheme: EventEmitter<string> = new EventEmitter<string>();

  private displayedColumns;

  private dataSource;

  constructor(private themesService: ThemeApiService) {
    this.displayedColumns = ['name', 'date', 'actions'];
  }

  ngOnInit() {
    this.getAllThemes();
  }

  getAllThemes() {
    this.themesService.readAll().subscribe(
      data => {
        this.dataSource = new MatTableDataSource<Theme>(data);
      },
      (error) => {
        this.dataSource = new MatTableDataSource<Theme>([]);
      });
  }
}
