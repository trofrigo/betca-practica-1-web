import {Component, Inject, OnInit} from '@angular/core';
import {ThemeApiService} from './theme-api.service';
import {Theme} from './theme';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-theme-delete',
  templateUrl: './theme-delete.component.html',
  styleUrls: ['./theme-delete.component.css']
})
export class ThemeDeleteComponent implements OnInit {

  private theme: Theme;

  constructor(private http: ThemeApiService,
              public dialogRef: MatDialogRef<ThemeDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.theme = data;
  }

  ngOnInit() {}

  delete() {
    this.http.delete(this.theme).subscribe(
      () => {
        this.dialogRef.close();
      });
  }
}
