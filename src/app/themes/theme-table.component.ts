import {Component, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ThemeDetailComponent} from './theme-detail.component';
import {ThemeApiService} from './theme-api.service';
import {Theme} from './theme';
import {ThemeListComponent} from './theme-list.component';
import {ThemeDeleteComponent} from './theme-delete.component';

@Component({
  selector: 'app-theme-table',
  templateUrl: './theme-table.component.html',
  styleUrls: ['./theme-table.component.css']
})
export class ThemeTableComponent {

  @ViewChild(ThemeListComponent) list;

  constructor(private http: ThemeApiService,
              public dialog: MatDialog) {
  }

  onCreateTheme() {
    this.openDetailDialog();
  }

  onEditTheme(theme: Theme) {
    this.http.read(theme.id).subscribe((data) => {
      this.openDetailDialog(data);
    });
  }

  onDeleteTheme(theme: Theme) {
    this.openDialog(ThemeDeleteComponent, {
      data: theme
    });
  }

  private openDetailDialog(data?: object) {

    let config;

    if (data) {
      config = {
        data: data
      };
    }

    this.openDialog(ThemeDetailComponent, config);
  }


  private openDialog(component: any, config: object) {
    const detailDialog = this.dialog.open(component, config);
    detailDialog.afterClosed().subscribe(() => {
      this.list.getAllThemes();
    });
  }
}
