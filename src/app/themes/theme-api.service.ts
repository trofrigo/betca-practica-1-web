import {Theme} from './theme';
import {Observable} from 'rxjs/Observable';
import {HttpService} from '../core/http.service';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';


@Injectable()
export class ThemeApiService {

  static ENDPOINT = '/themes';

  constructor(private http: HttpService) {
  }

  private errorMessages: object = {
    400: 'Comprueba que los campos son correctos',
    404: 'Tema no encontrado',
    409: 'Ya existe un tema con ese nombre'
  };

  readAll(): Observable<Theme[]> {
    return this.http.get(ThemeApiService.ENDPOINT)
      .map(res => <Theme[]>res);
  }

  read(id: String): Observable<Theme> {
    return this.http.get(`${ThemeApiService.ENDPOINT}/${id}`)
      .map(res => <Theme>res)
      .catch(this.http.notify(this.errorMessages));
  }

  create(theme: Theme): Observable<number> {
    return this.http.post(ThemeApiService.ENDPOINT, theme)
      .map(res => <number>res)
      .catch(this.http.notify(this.errorMessages));
  }

  update(theme: Theme): Observable<Theme> {
    return this.http.put(`${ThemeApiService.ENDPOINT}/${theme.id}`, theme)
      .map(res => <Theme>res)
      .catch(this.http.notify(this.errorMessages));
  }

  delete(theme: Theme): Observable<Theme> {
    return this.http.delete(`${ThemeApiService.ENDPOINT}/${theme.id}`)
      .map(res => <Theme>res);
  }
}
