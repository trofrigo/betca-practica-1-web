import {Observable} from 'rxjs/Observable';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';

@Injectable()
export class AccessGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const requiresLogin = route.data.requiresLogin || false;
    const requiresRole = route.data.requiresRole || false;

    if (requiresLogin && !this.authService.isAuthenticated()) {
      this.router.navigateByUrl('/login');
    }

    if (requiresRole && !this.authService.hasRights(requiresRole)) {
      this.router.navigateByUrl('/login');
    }

    return true;
  }
}
