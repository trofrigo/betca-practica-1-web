import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {

  static TOKEN = 'token';

  static ROLES = 'roles';

  public getToken(): string {
    return localStorage.getItem(AuthService.TOKEN);
  }

  public setToken(token) {
    localStorage.setItem(AuthService.TOKEN, token);
  }

  public setRoles(roles) {
    localStorage.setItem(AuthService.ROLES, JSON.stringify(roles));
  }

  public hasRights(roles): boolean {

    let requiredRoles = roles;
    const userRoles = JSON.parse(localStorage.getItem(AuthService.ROLES)) as Array<Role>;

    if (!Array.isArray(roles)) {
      requiredRoles = [roles];
    }

    return requiredRoles.some(role => userRoles.includes(role));
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    return !!token;
  }

  public logout() {
    this.setToken('');
    this.setRoles([]);
  }
}
