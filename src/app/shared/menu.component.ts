import {Component, OnInit} from '@angular/core';
import { NavigationEnd, Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  private showMenu: boolean;

  constructor(private authService: AuthService,
              private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.showMenu = this.router.url !== '/login' && this.router.url !== '/';
      }
    });
  }
}
