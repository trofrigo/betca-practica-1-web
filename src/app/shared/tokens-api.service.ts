import {Observable} from 'rxjs/Observable';
import {HttpService} from '../core/http.service';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Token} from './token';
import {HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';


@Injectable()
export class TokensApiService {

  static ENDPOINT = '/tokens';

  constructor(private http: HttpService, private snackBar: MatSnackBar) {}

  static getBasicAuthHeader(username, password) {
    const token = btoa(`${username}:${password}`);
    return `Basic ${token}`;
  }

  private errorMessages = {
      401: 'Credenciales incorrectos'
  }

  create(username: string, password: string): Observable<Token> {
    return this.http
      .post(TokensApiService.ENDPOINT, null, {
        headers: new HttpHeaders()
          .set('Authorization', TokensApiService.getBasicAuthHeader(username, password))
      })
      .map(res => <Token>res)
      .catch(this.http.notify(this.errorMessages));
  }


}
