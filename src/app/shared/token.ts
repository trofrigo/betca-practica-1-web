export interface Token {
  token: string;
  roles: Role[];
}
