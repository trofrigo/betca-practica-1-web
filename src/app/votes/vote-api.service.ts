import {Vote} from './vote';
import {Observable} from 'rxjs/Observable';
import {HttpService} from '../core/http.service';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {MatSnackBar} from '@angular/material';
import {VoteDto} from './vote-dto';


@Injectable()
export class VoteApiService {

  static ENDPOINT = '/votes';

  private errorMessages: object = {
      400: 'Comprueba que los campos son correctos',
      404: 'Voto no encontrado'
  }

  constructor(private http: HttpService) {
  }

  readAll(): Observable<Vote[]> {
    return this.http.get(VoteApiService.ENDPOINT)
      .map(res => <Vote[]>res);
  }

  read(id: String): Observable<Vote> {
    return this.http.get(`${VoteApiService.ENDPOINT}/${id}`)
      .map(res => <Vote>res)
      .catch(this.http.notify(this.errorMessages));
  }

  create(vote: VoteDto): Observable<number> {
    return this.http.post(VoteApiService.ENDPOINT, vote)
      .map(res => <number>res)
      .catch(this.http.notify(this.errorMessages));
  }

  update(vote: VoteDto): Observable<Vote> {
    return this.http.put(`${VoteApiService.ENDPOINT}/${vote.id}`, vote)
      .map(res => <Vote>res)
      .catch(this.http.notify(this.errorMessages));
  }

  delete(vote: Vote): Observable<Vote> {
    return this.http.delete(`${VoteApiService.ENDPOINT}/${vote.id}`)
      .map(res => <Vote>res);
  }
}
