import {Component, Inject, OnInit} from '@angular/core';
import {VoteApiService} from './vote-api.service';
import {Vote} from './vote';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-vote-delete',
  templateUrl: './vote-delete.component.html',
  styleUrls: ['./vote-delete.component.css']
})
export class VoteDeleteComponent implements OnInit {

  private vote: Vote;

  constructor(private http: VoteApiService,
              public dialogRef: MatDialogRef<VoteDeleteComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.vote = data;
  }

  ngOnInit() {}

  delete() {
    this.http.delete(this.vote).subscribe(
      () => {
        this.dialogRef.close();
      });
  }
}
