import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { VoteApiService } from './vote-api.service';
import { Vote } from './vote';
import { MatTableDataSource } from '@angular/material';


@Component({
  selector: 'app-vote-list',
  templateUrl: './vote-list.component.html',
  styleUrls: ['./vote-list.component.css']
})

export class VoteListComponent implements OnInit {

  @Output()
  editVote: EventEmitter<string> = new EventEmitter<string>();

  @Output()
  deleteVote: EventEmitter<string> = new EventEmitter<string>();

  private displayedColumns;

  private dataSource;

  constructor(private votesService: VoteApiService) {
    this.displayedColumns = ['name', 'date', 'actions'];
  }

  ngOnInit() {
    this.getAllVotes();
  }

  getAllVotes() {
    this.votesService.readAll().subscribe(
      data => {
        this.dataSource = new MatTableDataSource<Vote>(data);
      },
      (error) => {
        this.dataSource = new MatTableDataSource<Vote>([]);
      });
  }
}
