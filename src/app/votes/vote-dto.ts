export interface VoteDto {
  id?: String,
  value: number;
  themeId: String;
}
