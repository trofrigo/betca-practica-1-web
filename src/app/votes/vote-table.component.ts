import {Component, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {VoteDetailComponent} from './vote-detail.component';
import {VoteApiService} from './vote-api.service';
import {Vote} from './vote';
import {VoteListComponent} from './vote-list.component';
import {VoteDeleteComponent} from './vote-delete.component';
import {ThemeApiService} from '../themes/theme-api.service';
import {Theme} from '../themes/theme';

@Component({
  selector: 'app-vote-table',
  templateUrl: './vote-table.component.html',
  styleUrls: ['./vote-table.component.css']
})
export class VoteTableComponent {

  @ViewChild(VoteListComponent) list;

  constructor(private http: ThemeApiService,
              public dialog: MatDialog) {
  }

  onCreateVote() {
    this.http.readAll().subscribe((themes) => {
      this.openDetailDialog(themes);
    });
  }

  onEditVote(vote: Vote) {
    this.http.readAll().subscribe((themes) => {
      this.openDetailDialog(themes, vote);
    });
  }

  onDeleteVote(vote: Vote) {
    this.openDialog(VoteDeleteComponent, {
      data: vote
    });
  }

  private openDetailDialog(themes: Array<Theme>, vote?: object, ) {
      const config = {
        data: {
          themes,
          vote
        }
      };

    this.openDialog(VoteDetailComponent, config);
  }


  private openDialog(component: any, config: object) {
    const detailDialog = this.dialog.open(component, config);
    detailDialog.afterClosed().subscribe(() => {
      this.list.getAllVotes();
    });
  }
}
