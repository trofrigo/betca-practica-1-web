import {User} from '../shared/user';
import {Theme} from '../themes/theme';

export interface Vote {
  id: String;
  value: number;
  theme: Theme;
  user: User;
}
