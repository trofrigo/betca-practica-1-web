import {Component, Inject, OnInit} from '@angular/core';
import { VoteApiService } from './vote-api.service';
import {Vote} from './vote';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Theme} from '../themes/theme';
import {FormControl, Validators} from '@angular/forms';
import {VoteDto} from './vote-dto';

@Component({
  selector: 'app-vote-detail',
  templateUrl: './vote-detail.component.html',
  styleUrls: ['./vote-detail.component.css']
})
export class VoteDetailComponent implements OnInit {

  private vote: Vote;

  private themes: Array<Theme>;

  private themeControl: FormControl;

  constructor(
    private http: VoteApiService,
    public dialogRef: MatDialogRef<VoteDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    this.themes = data.themes || [];
    this.vote = data.vote || {
      id: null,
      theme: {
        id: null
      },
      value: null
    };
    this.themeControl = new FormControl(this.vote.theme.id, {
      validators: [Validators.required]
    });
  }

  ngOnInit() { }

  create() {
    if (this.checkForm()) {

      const vote: VoteDto = {
        value: this.vote.value,
        themeId: this.themeControl.value
      };

      this.http.create(vote).subscribe(
        () => { this.dialogRef.close(); });
    }

  }

  update() {
    if (this.checkForm()) {

      const vote: VoteDto = {
        id: this.vote.id,
        value: this.vote.value,
        themeId: this.themeControl.value
      };

      this.http.update(vote).subscribe(
        () => { this.dialogRef.close(); });
    }
  }

  private checkForm() {
    return (<HTMLFormElement>document.getElementById('detail-form')).checkValidity();
  }

  private isUpdating() {
    return !!this.vote.id;
  }

}
