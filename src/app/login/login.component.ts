import { Component, OnInit } from '@angular/core';
import { TokensApiService } from '../shared/tokens-api.service';
import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private username: string;

  private password: string;

  constructor(
    private http: TokensApiService,
    private authService: AuthService,
    private router: Router) { }

  onSingIn() {
    this.http.create(this.username, this.password)
      .subscribe((data) => {
        this.authService.setToken(data.token);
        this.authService.setRoles(data.roles);
        this.router.navigateByUrl('/votes');
      });
  }

  ngOnInit() {
    this.authService.logout();
  }
}
