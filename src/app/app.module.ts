import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {HttpService} from './core/http.service';

import {AppComponent} from './app.component';

import {ThemeTableComponent} from './themes/theme-table.component';
import {ThemeListComponent} from './themes/theme-list.component';
import {ThemeDetailComponent} from './themes/theme-detail.component';
import {ThemeDeleteComponent} from './themes/theme-delete.component';
import {ThemeApiService} from './themes/theme-api.service';

import {VoteTableComponent} from './votes/vote-table.component';
import {VoteListComponent} from './votes/vote-list.component';
import {VoteDetailComponent} from './votes/vote-detail.component';
import {VoteDeleteComponent} from './votes/vote-delete.component';
import {VoteApiService} from './votes/vote-api.service';

import {LoginComponent} from './login/login.component';

import {TokensApiService} from './shared/tokens-api.service';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule
} from '@angular/material';

import {CdkTableModule} from '@angular/cdk/table';
import {RouterModule, Routes} from '@angular/router';
import {AuthService} from './auth/auth.service';
import {TokenInterceptor} from './auth/token.interceptor';
import {AccessGuard} from './auth/access-guard';
import { MenuComponent } from './shared/menu.component';

const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [ AccessGuard ],
    data: {
      hideLogin: true
    }
  },
  {
    path: 'votes',
    component: VoteTableComponent,
    canActivate: [ AccessGuard ],
    data: {
      requiresLogin: true,
      requiresRole: ['ADMIN', 'USER']
    }
  },
  {
    path: 'themes',
    component: ThemeTableComponent,
    canActivate: [ AccessGuard ],
    data: {
      requiresLogin: true,
      requiresRole: ['ADMIN']
    }
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    ThemeTableComponent,
    ThemeListComponent,
    ThemeDetailComponent,
    ThemeDeleteComponent,
    VoteTableComponent,
    VoteListComponent,
    VoteDetailComponent,
    VoteDeleteComponent,
    LoginComponent,
    MenuComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    CdkTableModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    AccessGuard,
    ThemeApiService,
    VoteApiService,
    TokensApiService,
    AuthService,
    HttpService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  exports: [
    RouterModule
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ThemeDetailComponent,
    ThemeDeleteComponent,
    VoteDetailComponent,
    VoteDeleteComponent
  ],
})
export class AppModule {
}
