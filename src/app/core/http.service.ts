import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {MatSnackBar} from '@angular/material';


import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

  static URI = 'http://localhost:8081/api/v0';

  private params: HttpParams;

  private headers: HttpHeaders;

  constructor(private http: HttpClient,
              private snackBar: MatSnackBar) {
    this.initRequest();
  }

  private initRequest() {
    this.params = new HttpParams();
    this.headers = new HttpHeaders();
  }

  param(key: string, value: string): HttpService {
    this.params = this.params.set(key, value);
    return this;
  }

  header(key: string, value: string): HttpService {
    this.headers = this.headers.set(key, value);
    return this;
  }

  basicAuth(username: string, password: string): HttpService {
    const token = btoa(`${username}:${password}`);
    const header = `Basic ${token}`;

    return this.header('Authorization', header);
  }

  get(endpoint: string): Observable<any> {
    return this.http.get(HttpService.URI + endpoint, this.createOptions());
  }

  post(endpoint: string, body: Object, options?): Observable<any> {
    return this.http.post(HttpService.URI + endpoint, body, options);
  }

  delete(endpoint: string): Observable<any> {
    return this.http.delete(HttpService.URI + endpoint, this.createOptions());
  }

  put(endpoint: string, body?: Object): Observable<any> {
    return this.http.put(HttpService.URI + endpoint, body, this.createOptions());
  }

  patch(endpoint: string, body?: Object): Observable<any> {
    return this.http.patch(HttpService.URI + endpoint, body, this.createOptions());
  }

  private createOptions(): object {
    const options = {headers: this.headers, params: this.params};
    this.initRequest();
    return options;
  }

  private handleError(error: HttpErrorResponse, errorMessages) {
    const errorMessage = errorMessages[error.status] || 'En estos momentos no podemos conectar con el servidor, intentelo más tarde';
    this.snackBar.open(
      errorMessage,
      null,
      {
        duration: 3000
      });
    return Observable.throw(error);
  }

  public notify(errorMessages) {
    return (error: HttpErrorResponse) => {
      return this.handleError(error, errorMessages);
    };
  }
}
